#%%
import cv2
import numpy as np
import os
from PIL import Image
# %%
with open('./own_datapath.txt', 'r') as f:
    lines = f.readlines()
imagenames = [x.strip() for x in lines]
# %%
train_txt = open('./train.txt','w')
dataroot = '/home/mike/git/darknet/own_data/img/'
for l in imagenames:
    wordlist = l.split(' ')
    img = cv2.imread(wordlist[0])
    print(wordlist[0],img.shape)
    y,x,z = img.shape
    yolo_anno = []
    for coord_string in wordlist[1:]:
        coord = coord_string.split(',')
        x1 = int(coord[0])
        y1 = int(coord[1])
        x2 = int(coord[2])
        y2 = int(coord[3])
        c = int(coord[4])
        x_center  = (x1+x2)/2
        y_center  = (y1+y2)/2
        x_width = x2 - x1
        y_hight = y2 - y1
        # yolo_anno.append([ c, x_center/x, y_center/y, x_width/x, y_hight/y])
        yolo_anno.append([ c, x_center/x, y_center/y, x_width/x, y_hight/y])
    jpg_name = wordlist[0].split('/')[-1][:-4]
    img_txt = open(os.path.join(dataroot,f'{jpg_name}.txt'),'w')
    for anno in yolo_anno:
        print(anno)
        print(f'{anno[0]} {anno[1]:.6f} {anno[2]:.6f} {anno[3]:.6f} {anno[4]:.6f}',file=img_txt)
    img_txt.close()
    print(wordlist[0],file=train_txt)
train_txt.close()
# %%
' '.join(float(anno))
# %%
anno[0]
# %%
print(f'{anno[0]} {anno[1]:.6f} {anno[2]:.6f} {anno[3]} {anno[4]}')
# %%
imgpath ='/home/mike/git/darknet/own_data/img/S__50511895.jpg'
# %%
img = cv2.imread(imgpath)
# %%
y,x,z = img.shape

#(1774, 2364, 3)
# %%
/home/mike/git/darknet/own_data/img/S__50511895.jpg 548,698,1026,1170,0 1301,760,1785,1245,0

#%%
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import numpy as np

im = np.array(Image.open(imgpath), dtype=np.uint8)

# Create figure and axes
fig,ax = plt.subplots(1)

# Display the image
ax.imshow(im)

# Create a Rectangle patch
# x y width height
# (548,698),478,472
rect = patches.Rectangle((548,698),478,472,linewidth=1,edgecolor='r',facecolor='none')

# Add the patch to the Axes
ax.add_patch(rect)

plt.show()
# %%


a = [0.332910, 0.526494, 0.202200, 0.266065]
b = [0.652707, 0.565107, 0.204738. 0.273393]
# %%
# %%
 400,1161,781,1539,0 946,1238,1346,1652,0

#%%
imgpath ='/home/mike/git/darknet/own_data/img/S__50528283.jpg'
# %%
img = cv2.imread(imgpath)
# %%
y,x,z = img.shape
# %%
